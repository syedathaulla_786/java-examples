public class DataTypeCasting{
	
	public static void main(String[] args){
	
		int a=300;
		byte b= (byte) a;
		System.out.println("Casting int(300) to byte : "+b);
		
		byte c = 127;    // Storing Value greater than 127 gives error.
		int d =(int) c;
		System.out.println("Casting byte(127) to int : "+d);
	}
}